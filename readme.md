# Run VPN in docker

This script supports to run VPN with docker in minutes.

## Get started

- Clone this repository

```
    git clone https://gitlab.com/kingsleyht/docker-vpn.git
```

- Copy `config.sample.sh` to `config.sh` and edit what you want

```
cp config.sample.sh config.sh
```

- Run `install.sh` to setup
- Run `gen.sh KEY_NAME` to generate client key file

*Note* For easier, please make sure you includes config.sh in bash init script (actually it just need to have `$VPN_DATA` env)

## Todo

...

## Contribute

Free feel make pull request or email to `phuonghuynh.net@gmail.com`. Thank you!
