#!/bin/bash

set -e

if [ -z "$1" ]; then
  echo "Usage: $0 CLIENTNAME" >&2
  exit 1
fi

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
source "${DIR}/config.sh"
CLIENTNAME=$1
KEY_DIR="${VPN_ROOT}/keys"

if [ ! -d "${KEY_DIR}" ]; then
  mkdir -p "${KEY_DIR}"
fi

KEY_FILE="${KEY_DIR}/${CLIENTNAME}.ovpn"
if [[ -f "${KEY_FILE}" ]]; then
  echo "Key ${KEY_FILE} is already existed." >&2
  exit 1
fi

docker run -v "${VPN_DATA}":/etc/openvpn --rm -it kylemanna/openvpn easyrsa build-client-full "${CLIENTNAME}" nopass

# get key
docker run -v "${VPN_DATA}":/etc/openvpn --rm kylemanna/openvpn ovpn_getclient "${CLIENTNAME}" > "${KEY_FILE}"
